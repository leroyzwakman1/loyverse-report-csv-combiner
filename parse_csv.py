#!/usr/bin/python

import csv
import sys
from os import listdir
import os



# total arguments

class Bon:
    def __init__(self):
        self.datum = ""
        self.bonnr = ""
        self.bedrag = ""
        self.categorie = ""
        self.artikel = ""
        self.pos = ""

        self.betaalwijze = ""

n = len(sys.argv)
if n == 3:
    RECEIPTS_BY_ITEM = sys.argv[1]
    RECEIPTS = sys.argv[2]
else:
    filenames = listdir(".")
    for filename in filenames:
        if filename.endswith(".csv"):
            if filename.startswith("receipts-by-item"):
                RECEIPTS_BY_ITEM = filename
            if filename.startswith("receipts-2"):
                RECEIPTS = filename

bonnen = []
bon = None
with open(RECEIPTS_BY_ITEM, 'r') as file:
    reader = csv.DictReader(file)
    for row in reader:
        if row['Status'] == "Gesloten":
            bon = Bon()
            bon.datum = row['Datum']
            bon.bonnr = row['Bon nummer']
            bon.bedrag = row['Bruto-omzet']
            bon.categorie = row['Categorie']
            bon.pos = row['POS']
            bon.artikel = row['Artikel']

            bonnen.append(bon)

total = 0
omzet_cash  = dict()
total_cash = 0

omzet_pin  = dict()
total_pin = 0

omzet_personeel = dict()
total_personeel = 0

with open(RECEIPTS, 'r') as file:
    reader = csv.DictReader(file)
    for row in reader:
       for bon in bonnen:
           if bon.bonnr == row['Bon nummer']:
              bon.betaalwijze = row['Betaalwijzen']
              # Check for Pin / Sumup
              if "," in bon.betaalwijze:
                print("! meerdere betaalwijze op bon " + bon.bonnr + " splits deze regels")
                sys.exit(1)
              #print(bon.betaalwijze)
              if bon.betaalwijze.startswith("PIN") or bon.betaalwijze.startswith("Sum"):
                if bon.categorie not in omzet_pin:
                   omzet_pin[bon.categorie] = float(bon.bedrag)
                else:
                  omzet_pin[bon.categorie] = omzet_pin[bon.categorie] + float(bon.bedrag)
                total_pin += float(bon.bedrag)
              # Check for Cash
              elif bon.betaalwijze.startswith("Cash"):
                if bon.betaalwijze.startswith("Cash"):
                  if bon.categorie not in omzet_cash:
                      omzet_cash[bon.categorie] = float(bon.bedrag)
                  else:
                    omzet_cash[bon.categorie] = omzet_cash[bon.categorie] + float(bon.bedrag)
                  total_cash += float(bon.bedrag)
              # Check for Personell
              elif bon.betaalwijze.startswith("Vrij"):
                if bon.categorie not in omzet_personeel:
                    omzet_personeel[bon.categorie] = float(bon.bedrag)
                else:
                  omzet_personeel[bon.categorie] = omzet_personeel[bon.categorie] + float(bon.bedrag)
                total_personeel += float(bon.bedrag)
              # we don't know!!
              else:
                print("onbekend: " + row)
                print(bon.datum + "," +
                      bon.pos + "," +
                      bon.bonnr + "," +
                      bon.artikel + "," +
                      bon.categorie + "," +
                      bon.betaalwijze + "," +
                      bon.bedrag)

f_pin = open("_pin.csv", "a")
f_cash = open("_cash.csv", "a")

print("---------------------------------------------------------")
print("Periode: " + bonnen[len(bonnen)-1].datum + " t/m " + bonnen[0].datum)
print("---------------------------------------------------------")
print ("Verbruik Personeel: %5.2f" % round(total_personeel, 2) )
controle = 0
for personeel in omzet_personeel:
    print ("\t" + personeel + " : %5.2f" %  omzet_personeel[personeel])
    controle += omzet_personeel[personeel]
print ("controle: %5.2f" % controle)
print("---------------------------------------------------------")

print ("Totaal Contant: %5.2f" % round(total_cash, 2) )
controle = 0
for cash in omzet_cash:
    print ("\t" + cash + " : %5.2f" %  omzet_cash[cash])
    controle += omzet_cash[cash]
print ("Controle: %5.2f" % controle)
print("---------------------------------------------------------")
print ("Totaal PIN: %5.2f" % round(total_pin, 2) )
controle = 0
for pin in omzet_pin:
    print ("\t" + pin + " : %5.2f" %  omzet_pin[pin])
    controle += omzet_pin[pin]
print ("controle: %5.2f" % controle)
print("---------------------------------------------------------")
print("")
print("---------------------------------------------------------")
print("Omzet Cash na samenvoegen")
print("---------------------------------------------------------")

datum=os.path.basename(RECEIPTS).replace("receipts-", "")[0:10]
print(datum)

omzet_kantine  = 0
if "Snacks" in omzet_cash:
    omzet_kantine  += omzet_cash['Snacks']
if "Dranken" in omzet_cash:
    omzet_kantine  += omzet_cash['Dranken']
f_cash.write("{},{},{},{}\n".format( datum, 1003, 8101, round(omzet_kantine, 2)))

omzet_baan = 0
if "Kaarten" in omzet_cash:
    omzet_baan += omzet_cash['Kaarten']
if "Munitie" in omzet_cash:
    omzet_baan += omzet_cash['Munitie']
f_cash.write("{},{},{},{}\n".format( datum, 1003, 8102, round(omzet_baan, 2)))

omzet_wedstrijden = 0
if "Wedstrijden" in omzet_cash:
    omzet_wedstrijden += omzet_cash['Wedstrijden']
f_cash.write("{},{},{},{}\n".format( datum, 1003, 8103, round(omzet_wedstrijden, 2)))

omzet_clubcompetitie = 0
if "Clubcompetitie" in omzet_cash:
    omzet_clubcompetitie += omzet_cash['Clubcompetitie']
f_cash.write("{},{},{},{}\n".format( datum, 1003, 8104, round(omzet_clubcompetitie, 2)))

omzet_aspiranten = 0
if "Aspirant" in omzet_cash:
    omzet_aspiranten += omzet_cash['Aspirant']
f_cash.write("{},{},{},{}\n".format( datum, 1003, 8002, round(omzet_aspiranten, 2)))

omzet_lesgeld = 0
if "Lesgeld" in omzet_cash:
    omzet_lesgeld += omzet_cash['Lesgeld']
f_cash.write("{},{},{},{}\n".format( datum, 1003, 8001, round(omzet_lesgeld, 2)))

omzet_diversen = 0
if "Diversen" in omzet_cash:
    omzet_diversen += omzet_cash['Diversen']
f_cash.write("{},{},{},{}\n".format( datum, 1003, 8107, round(omzet_diversen, 2)))

omzet_fooi = 0
if "Fooi" in omzet_cash:
    omzet_fooi += omzet_cash['Fooi']
f_cash.write("{},{},{},{}\n".format( datum, 1003, 8201, round(omzet_fooi, 2)))

omzet_clinics = 0
if "Clinics" in omzet_cash:
    omzet_clinics += omzet_cash['Clinics']
f_cash.write("{},{},{},{}\n".format( datum, 1003, 8105, round(omzet_clinics, 2)))

print ("total cash: %5.3f" % total_cash)
print ("\tOmzet Kantine: %5.3f" %  omzet_kantine)
print ("\tOmzet Baan: %5.3f" %  omzet_baan)
print ("\tOmzet Wedstrijden: %5.3f" %  omzet_wedstrijden)
print ("\tOmzet Clubcompetitie: %5.3f" %  omzet_clubcompetitie)
print ("\tOmzet Aspiranten: %5.3f" %  omzet_aspiranten)
print ("\tOmzet Lesgeld: %5.3f" %  omzet_lesgeld)
print ("\tOmzet Diversen: %5.3f" %  omzet_diversen)
print ("\tOmzet Clinics: %5.3f" %  omzet_clinics)
print ("\tOmzet Fooi: %5.3f" %  omzet_fooi)

print("---------------------------------------------------------")
print("Omzet SumUp na samenvoegen en -1.9%")
print("---------------------------------------------------------")
total_pin = total_pin * 0.981


omzet_kantine  = 0
if "Snacks" in omzet_pin:
    omzet_kantine  += omzet_pin['Snacks']
if "Dranken" in omzet_pin:
    omzet_kantine  += omzet_pin['Dranken']
omzet_kantine = round(omzet_kantine  * .981, 2)
f_pin.write("{},{},{},{}\n".format( datum, 1090, 8101, omzet_kantine))

omzet_baan = 0;
if "Kaarten" in omzet_pin:
    omzet_baan += omzet_pin['Kaarten']
if "Munitie" in omzet_pin:
    omzet_baan += omzet_pin['Munitie']
omzet_baan = round(omzet_baan  * .981, 2)
f_pin.write("{},{},{},{}\n".format( datum, 1090, 8102, omzet_baan))

omzet_wedstrijden = 0
if "Wedstrijden" in omzet_pin:
    omzet_wedstrijden += omzet_pin['Wedstrijden']
omzet_wedstrijden = round(omzet_wedstrijden  * .981, 2)
f_pin.write("{},{},{},{}\n".format( datum, 1090, 8103, omzet_wedstrijden))

omzet_clubcompetitie = 0
if "Clubcompetitie" in omzet_pin:
    omzet_clubcompetitie += omzet_pin['Clubcompetitie']
omzet_clubcompetitie = round(omzet_clubcompetitie  * .981, 2)
f_pin.write("{},{},{},{}\n".format( datum, 1090, 8104, omzet_clubcompetitie))

omzet_aspiranten = 0
if "Aspirant" in omzet_pin:
    omzet_aspiranten += omzet_pin['Aspirant']
omzet_aspiranten = round(omzet_aspiranten  * .981, 2)
f_pin.write("{},{},{},{}\n".format( datum, 1090, 8002, omzet_aspiranten))

omzet_lesgeld = 0
if "Lesgeld" in omzet_pin:
    omzet_lesgeld += omzet_pin['Lesgeld']
omzet_lesgeld = round(omzet_lesgeld  * .981, 2)
f_pin.write("{},{},{},{}\n".format( datum, 1090, 8001, omzet_lesgeld))

omzet_diversen = 0
if "Diversen" in omzet_pin:
    omzet_diversen += omzet_pin['Diversen']
omzet_diversen = round(omzet_diversen  * .981, 2)
f_pin.write("{},{},{},{}\n".format( datum, 1090, 8107, omzet_diversen))

omzet_fooi = 0
if "Fooi" in omzet_pin:
    omzet_fooi += omzet_pin['Fooi']
omzet_fooi = round(omzet_fooi  * .981, 2)
f_pin.write("{},{},{},{}\n".format( datum, 1090, 8201, omzet_fooi))

omzet_clinics = 0
if "Clinics" in omzet_pin:
    omzet_clinics += omzet_pin['Clinics']
omzet_clinics = round(omzet_clinics * .981, 2)
f_pin.write("{},{},{},{}\n".format( datum, 1090, 8105, omzet_clinics))

print ("total pin: %5.3f" % total_pin)
print ("\tOmzet Kantine: %5.3f" %  omzet_kantine)
print ("\tOmzet Baan: %5.3f" %  omzet_baan)
print ("\tOmzet Wedstrijden: %5.3f" %  omzet_wedstrijden)
print ("\tOmzet Clubcompetitie: %5.3f" %  omzet_clubcompetitie)
print ("\tOmzet Aspiranten: %5.3f" %  omzet_aspiranten)
print ("\tOmzet Lesgeld: %5.3f" %  omzet_lesgeld)
print ("\tOmzet Diversen: %5.3f" %  omzet_diversen)
print ("\tOmzet Clinics: %5.3f" %  omzet_clinics)
print ("\tOmzet Fooi: %5.3f" %  omzet_fooi)
print("---------------------------------------------------------")
f_pin.close()
f_cash.close()
