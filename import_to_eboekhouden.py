#!/usr/bin/python

"""
Script to inject mutatie into e-boekhouden
"""
import os
import csv
import json
import sys

from zeep import Client

n = len(sys.argv)
if n == 2:
    filename = sys.argv[1]
else:
    print("need csv file")
    sys.exit(1)

def read_secrets() -> dict:
    """
    reads the secrest from json file
    """
    filename = os.path.join('secrets.json')
    try:
        with open(filename, mode='r') as f:
            return json.loads(f.read())
    except FileNotFoundError:
        return {}

secrets = read_secrets()


client = Client('https://soap.e-boekhouden.nl/soap.asmx?wsdl')

session = client.service.OpenSession(
    secrets['USERNAME'],
    secrets['SECRET1'],
    secrets['SECRET2'],
    'zeep'
    )

sessionid = session.SessionID

cMutRegelArray = client.get_type('ns0:ArrayOfCMutatieRegel')
cMutRegelArray = cMutRegelArray()


def add_mutatie_regel(bedrag, rekening):
    """
    Generates mutatie regel based on bedrag and rekeningnr

    Parameters
    ----------
    bedrag : float
       bedrag voor mutatieregel
    rekening : string
        rekening nummer voor mutatieregel
    """

    global cMutRegelArray
    c_mut_regel = client.get_type('ns0:cMutatieRegel')
    tmp_mut_regel = c_mut_regel (
        BedragInvoer=bedrag,
        BedragExclBTW=bedrag,
        BedragBTW=0,
        BTWCode="GEEN",
        BedragInclBTW=bedrag,
        BTWPercentage=0,
        KostenplaatsID=0,
        TegenrekeningCode=rekening
    )
    cMutRegelArray['cMutatieRegel'].append(tmp_mut_regel)

datum=""
reknr=0
with open(filename) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:
        if datum != row[0]:
            if datum != "":
               cMut = client.get_type('ns0:cMutatie')
               cMutatie = cMut(
                   MutatieNr=0,
                   Datum=datum,
                   Soort="GeldOntvangen",
                   Omschrijving=omschrijving,
                   Rekening=reknr,
                   InExBTW="EX",
                   MutatieRegels=cMutRegelArray
               )
               session = client.service.AddMutatie(sessionid, secrets['SECRET2'], cMutatie)
               print(session)

               datum = row[0]
               cMutRegelArray = client.get_type('ns0:ArrayOfCMutatieRegel')
               cMutRegelArray = cMutRegelArray()
            else:
               datum = row[0]
               cMutRegelArray = client.get_type('ns0:ArrayOfCMutatieRegel')
               cMutRegelArray = cMutRegelArray()

        if float(row[3]) > 0:
            add_mutatie_regel(row[3], row[2])
            reknr=int(row[1])
            if reknr == 1003:
                omschrijving="Verkopen Contant"
            else:
                omschrijving="Verkopen PIN"

cMut = client.get_type('ns0:cMutatie')
cMutatie = cMut(
    MutatieNr=0,
    Datum=datum,
    Soort="GeldOntvangen",
    Rekening=int(reknr),
    Omschrijving=omschrijving,
    InExBTW="EX",
    MutatieRegels=cMutRegelArray
)


session = client.service.AddMutatie(sessionid, secrets['SECRET2'], cMutatie)
print(session)
print("do logic andsend")
print("einde")



session = client.service.CloseSession(sessionid)
