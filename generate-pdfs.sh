rm *csv

while read csv ; do
  date=$(echo $csv | grep -Po '2...-..-..' | uniq)
  csv1=$csv
  csv2=$(echo $csv | sed -e 's/-by-item//g')
  #echo $csv1 - $csv2 - $date
  #loyverse-csv-sumup.py $csv1 $csv2 | tee out
  ./parse_csv.py $csv1 $csv2 | tee out
  cat out | text2pdf > /tmp/loyverse-${date}.pdf
done < <(find ~/Downloads -maxdepth 1 -name "receipts-by-item*.csv" |sort )
#sed -i -e '/,0$/d' _pin.csv
#sed -i -e '/,0$/d' _cash.csv
#sed -i -e '/0.0$/d' _pin.csv
#sed -i -e '/0.0$/d' _cash.csv
#done < <(find . -maxdepth 1 -name "receipts-by-item*.csv" )
